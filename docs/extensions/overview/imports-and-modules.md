---
title: Imports and Modules
---

# Imports and Modules

::: warning
GNOME Shell and Extensions use ESModules as of GNOME 45. Please see the
[Legacy Documentation][legacy-imports] for previous versions.
:::

There are effectively two types of modules that can imported in GJS, the most
common being libraries in the GNOME platform, and the other being JavaScript
files.

[legacy-imports]: ../upgrading/legacy-documentation.md#imports-and-modules

## Exporting Modules

::: tip
The [`Extension`] and [`ExtensionPreferences`] subclasses must be marked as
the `default export`.
:::

Larger extensions or extensions with discrete components often separate code
into modules. You can use the [`export`] declaration to mark classes, functions
and variables for export.

<<< @/../src/extensions/overview/imports-and-modules/exportingModules.js{js}

[`export`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/export
[`Extension`]: ../topics/extension.md#extension
[`ExtensionPreferences`]: ../topics/extension.md#extensionpreferences

## Importing Modules

Imports in your extension's directory can be imported using a relative path. For
example, if the module above were placed in `example@gjs.guide/utils.js` the
script above would be available at `./utils.js`.

GNOME Shell modules are bundled in a GResource and the [`import`] statement must
use a `resource://` URI. Note that extension code (i.e. `extension.js`) uses the
base path `resource:///org/gnome/shell/js/`, while preferences (i.e. `prefs.js`)
uses the base path `resource:///org/gnome/Shell/Extensions/js/`.

<<< @/../src/extensions/overview/imports-and-modules/importingModules.js{js}

Many of the elements in GNOME Shell like panel buttons, popup menus and
notifications are built from reusable classes and functions, found in modules
like these:

* [`js/ui/modalDialog.js`]
* [`js/ui/popupMenu.js`]
* [`js/ui/quickSettings.js`]

You can browse around in the `js/ui/` folder or any other JavaScript file under
`js/` for more code to be reused. Notice the path structure in the links above
and how they compare to the imports below:

```js
import * as ModalDialog from 'resource:///org/gnome/shell/ui/modalDialog.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';
```

[`import`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/import
[`js/ui/modalDialog.js`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/modalDialog.js
[`js/ui/popupMenu.js`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/popupMenu.js
[`js/ui/quickSettings.js`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/quickSettings.js

## Importing Libraries

Extensions can import libraries from the GNOME platform, or any other library
supporting [GObject Introspection]. There are also a few built-in libraries
such as [`Cairo`] and [`System`] that are imported differently.

<<< @/../src/extensions/overview/imports-and-modules/importingLibraries.js{js}

[GObject Introspection]: https://gi.readthedocs.io/en/latest/index.html
[`Cairo`]: https://gjs-docs.gnome.org/gjs/cairo.md
[`System`]: https://gjs-docs.gnome.org/gjs/system.md

