---
title: Preferences
---

# Preferences

::: warning
This documentation is for GNOME 45 and later. Please see the
[Legacy Documentation][legacy-preferences] for previous versions.
:::

Creating a preferences window for your extension allows users to configure the
appearance and behavior of the extension. It can also contain documentation,
a changelog and other information.

The user interface will be created with [GTK4] and [Adwaita], which has many
elements specifically for settings and configuration. You may consider looking
through the GNOME Human Interface Guidelines, or widget galleries for ideas.

[legacy-preferences]: ../upgrading/legacy-documentation.md#preferences
[GTK4]: https://gjs-docs.gnome.org/gtk40/
[Adwaita]: https://gjs-docs.gnome.org/adw1/

#### See Also

* [GNOME HIG](https://developer.gnome.org/hig)
* [GTK4 Widget Gallery](https://docs.gtk.org/gtk4/visual_index.html)
* [Adwaita Widget Gallery](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/widget-gallery.html)

## GSettings

[GSettings] provides a simple, extremely fast API for storing
application settings, that can also be used by GNOME Shell extensions.

[GSettings]: https://gjs-docs.gnome.org/gio20/gio.settings

### Creating a Schema

::: tip
A GSettings schema ID with the prefix `org.gnome.shell.extensions` and a path
with the prefix `/org/gnome/shell/extensions` is the standard for extensions.
:::

Schema files describe the types and default values of a particular group of
settings, using the same type format as [GVariant]. The basename of the schema
file should be the same as the schema ID.

Start by creating a `schemas/` subdirectory to hold the GSettings schema:

```sh:no-line-numbers
$ mkdir -p ~/.local/share/gnome-shell/extensions/example@gjs.guide/schemas
$ cd ~/.local/share/gnome-shell/extensions/example@gjs.guide
$ touch schemas/org.gnome.shell.extensions.example.gschema.xml
```

Then using your editor, create a schema describing the keys and type of values
they can hold:

<<< @/../src/extensions/development/preferences/schemas/org.gnome.shell.extensions.example.gschema.xml{xml}

[GVariant]: https://docs.gtk.org/glib/gvariant-format-strings.html

### Compiling a Schema

::: tip
As of GNOME 44, settings schemas are compiled automatically for extensions
installed with the `gnome-extensions` tool, [GNOME Extensions] website, or a
compatible application like [Extension Manager].
:::

Before it can be used, a GSettings schema must be compiled. If not using the
`gnome-extensions` tool, `glib-compile-schemas` can be used to compile schemas:

```sh:no-line-numbers
$ cd ~/.local/share/gnome-shell/extensions/example@gjs.guide
$ glib-compile-schemas schemas/
```

[GNOME Extensions]: https://extensions.gnome.org
[Extension Manager]: https://flathub.org/apps/com.mattjakeman.ExtensionManager

## Integrating Settings

::: tip
For complex settings, see the [GVariant Guide] for examples of what data types
can be stored with GSettings.
:::

GSettings is supported by a backend, which allows different processes to share
read-write access to the same settings values. This means that while
`extension.js` is reading and applying settings, `prefs.js` can be reading and
modifying them.

Usually this is a one-way relationship, but it is possible to change settings
from `extension.js` as well, if necessary.

### `metadata.json`

The recommended method for defining the schema ID for an extension is by
defining the [`settings-schema`](../overview/anatomy.md#settings-schema) key in
`metadata.json`. This allows GNOME Shell to automatically use the correct
schema ID when `ExtensionsBase.prototype.getSettings()` is called.

<<< @/../src/extensions/development/preferences/metadata.json{8 json}

Otherwise, you should call `ExtensionBase.prototype.getSettings()` with a
valid GSettings schema ID.

::: details <code>getSettings()</code> in <code>extension.js</code>
<<< @/../src/extensions/development/preferences/getSettingsExtension.js{6 js}
:::

::: details <code>getSettings()</code> in <code>prefs.js</code>
<<< @/../src/extensions/development/preferences/getSettingsPreferences.js{6 js}
:::

### `extension.js`

Methods like [`Gio.Settings.get_boolean()`] are used for native values, while
methods like [`Gio.Settings.set_value()`] can be used to work with
`GLib.Variant` values directly.

Simple types like `Boolean` can be bound directly to a [GObject Property] with
[`Gio.Settings.bind()`]. For JavaScript properties and other use cases, you can
connect to [`Gio.Settings::changed`] with the property name as the signal detail
(e.g. `changed::show-indicator`).

<<< @/../src/extensions/development/preferences/extension.js{24-37,43 js}


[GVariant Guide]: ../../guides/glib/gvariant.md
[GObject Property]: ../../guides/gobject/basics.md#gobject-properties
[`Gio.Settings.bind()`]: https://gjs-docs.gnome.org/gio20/gio.settings#method-bind
[`Gio.Settings.get_boolean()`]: https://gjs-docs.gnome.org/gio20/gio.settings#method-get_boolean
[`Gio.Settings.set_value()`]: https://gjs-docs.gnome.org/gio20/gio.settings#method-set_value
[`Gio.Settings::changed`]: https://gjs-docs.gnome.org/gio20/gio.settings#signal-changed

### `prefs.js`

::: tip
Extension preferences run in a separate process, without access to code in
GNOME Shell, and are written with [GTK4] and [Adwaita].
:::

The [Adwaita Widget Gallery] has screenshots of the many widgets it includes
that make building a preferences dialog easier. You may also use any of the
[GNOME APIs](https://gjs-docs.gnome.org) that are compatible with [GTK4]
(notable exceptions include `Meta`, `Clutter`, `Shell` and `St`).

Extensions should implement `fillPreferencesWindow()`, which is passed a new
instance of [`Adw.PreferencesWindow`] before it is displayed to the user.

<<< @/../src/extensions/development/preferences/prefs.js{js}

[`Adw.PreferencesWindow`]: https://gjs-docs.gnome.org/adw1/adw.preferenceswindow
[Adwaita Widget Gallery]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/widget-gallery.html

## Testing the Preferences

The preference dialog can be opened with `gnome-extensions prefs`, or any other
tool for managing extensions:

<img src="/assets/img/gnome-extensions-example-prefs.png" />

### Debugging `prefs.js`

Because preferences are not run within `gnome-shell` but in a separate process,
the logs will appear in the `gjs` process:

```sh:no-line-numbers
$ journalctl -f -o cat /usr/bin/gjs
```

### Debugging GSettings

To help debug the changes made by `prefs.js` to GSettings values, you can use
`dconf` to watch the path for your settings:

```sh:no-line-numbers
$ dconf watch /org/gnome/shell/extensions/example
```

## Next Steps

By now you should have a simple extension, ready to accept translations into
other languages, with basic preferences.

As your extension evolves you may want to customize the user interface by
creating your own widgets. If so, you should familiarize yourself with basic
[Accessibility](accessibility.md) concepts.
