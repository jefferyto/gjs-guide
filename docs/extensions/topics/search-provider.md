---
title: Search Provider
---

# Search Provider

::: warning
This documentation is for GNOME 45 and later. Please see the
[Legacy Documentation][legacy-searchprovider] for previous versions.
:::

A search provider is a mechanism by which an application can expose its search
capabilities to GNOME Shell. Text from the search entry is forwarded to all
search providers, which may each return a set of search results.

Applications must register search providers by exporting a D-Bus service, while
GNOME Shell extensions can register search providers directly.

[legacy-searchprovider]: ../upgrading/legacy-documentation.md#search-provider

#### See Also

* [GNOME Developer Documentation](https://developer.gnome.org/documentation/tutorials/search-provider.html)

## Imports

The following imports should be all most developers need for Search Providers:

<<< @/../src/extensions/topics/search-provider/extension.js#imports{js}


## Example Usage

GNOME Shell extensions create search providers by creating a class implementing
a [simple interface](#searchprovider). This class is responsible for returning a
list of results for a list of search terms.

Results are returned as unique string identifiers, which may be passed back to
the search provider to request [`ResultMeta`](#resultmeta) objects. These are
used by GNOME Shell to populate the results displayed to the user.

Search providers are constructed and then [registered](#registration) with GNOME
Shell, before they start receiving search requests.

### `CreateIcon`

The `CreateIcon` callback is a property of a [`ResultMeta`](#resultmeta), used
to return a [`Clutter.Actor`] to represent a search result. Usually this will
be an [`St.Icon`], and the scale factor should be accounted for.

Implementations can call [`St.ThemeContext.get_for_stage()`] on [`global.stage`]
to get the current scale and use it as a multiplier for the `size` argument.

<<< @/../src/extensions/topics/search-provider/extension.js#create-icon{js}


[`Clutter.Actor`]: https://gjs-docs.gnome.org/clutter13/clutter.actor
[`St.Icon`]: https://gjs-docs.gnome.org/st13/st.icon
[`St.ThemeContext.get_for_stage()`]: https://gjs-docs.gnome.org/st13/st.themecontext#method-get_for_stage
[`global.stage`]: https://gjs-docs.gnome.org/shell13/shell.global#property-stage

### `ResultMeta`

The `ResultMeta` object is a light-weight metadata object, used to represent a
search result in the search view. Search providers must return objects of this
type when `SearchProvider.prototype.getResultMetas()` is called.

<<< @/../src/extensions/topics/search-provider/extension.js#result-meta{js}


The `id` is a unique ID for the result identifier, that may be passed as an
argument to `SearchProvider.prototype.activateResult()`.

The `description` property is optional, holding a longer description of the
result that is only displayed in the list view.

The `clipboardText` property is optional, holding text that will be copied to
the clipboard if the result is activated.

### `SearchProvider`

Below is a fully documented implementation of a search provider class. An
extension may implement this interface as part of its default export, so long
as it still implements `enable()` and `disable()`.

<<< @/../src/extensions/topics/search-provider/extension.js#search-provider{js}

### Registration

Search providers from GNOME Shell extensions must be registered before they
will receive search queries. Registration should be performed in the extension's
`enable()` method and unregistered in `disable()`.

<<< @/../src/extensions/topics/search-provider/extension.js#registration{js}
