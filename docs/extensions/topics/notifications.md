---
title: Notifications
---

# Notifications

GNOME Shell provides implementations for [freedesktop.org Notifications][fdo]
and [GTK Notifications][gtk], serving as the notification server. These
different implementations are usually hidden from application developers, who
use the [`Gio.Notification`][gnotification] abstraction to send notifications.

The GNOME Shell process itself is not a `Gio.Application`, and uses its own
internal methods for showing notifications, bypassing the notification server.
Extensions may also use these methods for displaying notifications.

[fdo]: https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html
[gtk]: https://developer.gnome.org/documentation/tutorials/notifications.html
[gnotification]: https://gjs-docs.gnome.org/gio20/gio.notification

## Imports

The following imports should be all most developers need for Notifications:

<<< @/../src/extensions/topics/notifications/extension.js#imports{js}

## Simple Notifications

Extensions that want to display a simple notification to the user, may use the
method `Main.notify()`.

<<< @/../src/extensions/topics/notifications/extension.js#main-notify{js}

If the notification is communicating an error to the user, the
`Main.notifyError()` method will also log the notification as a warning.

<<< @/../src/extensions/topics/notifications/extension.js#main-notify-error{js}

The logged warning will appear similar to this:

```sh
GNOME Shell-Message: 00:00:00.000: error: Failed to load configuration: File not found
```

## Notifications

The [`MessageTray.Notification`] class represents a notification, which is
usually an event from an application or system component.

<<< @/../src/extensions/topics/notifications/extension.js#notification-new{js}

When a notification is acknowledged in a permanent way, it will emit the
`destroy` signal with a reason:

<<< @/../src/extensions/topics/notifications/extension.js#notification-destroy{js}

The `reason` argument may be one of the following:

* `MessageTray.NotificationDestroyedReason.EXPIRED`
  
  The notification was dismissed without being acknowledged by the user.
* `MessageTray.NotificationDestroyedReason.DISMISSED`
  
  The notification was closed by the user.
* `MessageTray.NotificationDestroyedReason.SOURCE_CLOSED`
  
  The notification was closed by its `MessageTray.Source`.
* `MessageTray.NotificationDestroyedReason.REPLACED`
  
  The notification was replaced by a newer version.

### Actions

Every notification has a default action that is invoked when the notification
itself is activated.

<<< @/../src/extensions/topics/notifications/extension.js#action-default{js}

Notifications may also have 1-3 action buttons. Each button is added by
calling `addAction()`, which takes a label and a callback (with no arguments):

<<< @/../src/extensions/topics/notifications/extension.js#action-add{js}

A notification's actions can be removed with `clearActions()`:

<<< @/../src/extensions/topics/notifications/extension.js#action-clear{js}

[`MessageTray.Notification`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/messageTray.js

### Urgency

The `urgency` property controls how and when notifications are presented to the
user. Since there may be a limit to how many notifications a source may show,
a notification may replace another with a lower `urgency`.

 may be one of the following:

* `MessageTray.Urgency.LOW`

  These notifications will be shown in the tray, but will not popup on the
  screen or be expanded.
* `MessageTray.Urgency.NORMAL`

  These notifications will be shown in the tray, and will popup on the screen
  unless the policy forbids it.
* `MessageTray.Urgency.HIGH`

  These notifications will be shown in the tray, and will popup on the screen
  unless the policy forbids it.
* `MessageTray.Urgency.CRITICAL`

  These notifications will always be shown, with the banner expanded, and must
  be acknowledged by the user before they will be dismissed.

## Sources

The [`MessageTray.Source`] class represents a source of notifications, like an
application or other desktop component. The simple notification helpers use the
default system source:

<<< @/../src/extensions/topics/notifications/extension.js#source-system{js}

An extension may create a custom source for managing its notifications, but
must connect to the `destroy` signal to safely reuse it.

<<< @/../src/extensions/topics/notifications/extension.js#source-custom{js}

[`MessageTray.Source`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/messageTray.js

### Policies

The [`MessageTray.NotificationPolicy`] class represents a policy that dictates
how and when notifications are presented for a source. For example, whether to
show on the lock screen or enable sound.

A custom notification policy is created by subclassing and overriding the
read-only property getters:

<<< @/../src/extensions/topics/notifications/extension.js#policy{js}

The default policy is `MessageTray.NotificationGenericPolicy`, which follows
the desktop settings for notifications.

[`MessageTray.NotificationPolicy`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/messageTray.js
