import GLib from 'gi://GLib';


// Expected output here is:
//   "one","two"
const variantStrv = GLib.Variant.new_strv(['one', 'two']);
print(variantStrv.deepUnpack());


// Expected result here is:
//   {
//     "key1": "value1",
//     "key2": "value2"
//   }
const shallowDict = new GLib.Variant('a{ss}', {
    'key1': 'value1',
    'key2': 'value2',
});

const shallowDictUnpacked = shallowDict.deepUnpack();


// Expected result here is:
//   {
//     "key1": [object variant of type "s"],
//     "key2": [object variant of type "b"]
//   }
const deepDict = new GLib.Variant('a{sv}', {
    'key1': GLib.Variant.new_string('string'),
    'key2': GLib.Variant.new_boolean(true),
});

const deepDictUnpacked = deepDict.deepUnpack();
