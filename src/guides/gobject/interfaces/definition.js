import GObject from 'gi://GObject';
import Gio from 'gi://Gio';


const SimpleInterface = GObject.registerClass({
    GTypeName: 'SimpleInterface',
    Requires: [GObject.Object],
    Properties: {
        'simple-property': GObject.ParamSpec.boolean(
            'simple-property',
            'Simple property',
            'A property that must be implemented',
            GObject.ParamFlags.READABLE,
            true
        ),
    },
    Signals: {
        'simple-signal': {},
    },
}, class SimpleInterface extends GObject.Interface {
    /**
     * By convention interfaces provide methods for emitting their signals, but
     * you can always call `emit()` on the instance of an implementation.
     */
    emitSimple() {
        this.emit('simple-signal');
    }

    /**
     * Interfaces can define methods that MAY be implemented, by providing a
     * default implementation.
     */
    optionalMethod() {
        return true;
    }

    /**
     * Interfaces can define methods that MUST be implemented, by throwing the
     * special error `GObject.NotImplementedError()`.
     */
    requiredMethod() {
        throw new GObject.NotImplementedError();
    }
});


const SimpleImplementation = GObject.registerClass({
    Implements: [SimpleInterface],
    Properties: {
        'simple-property': GObject.ParamSpec.override('simple-property',
            SimpleInterface),
    },
}, class SimpleImplementation extends GObject.Object {
    get simple_property() {
        return true;
    }

    requiredMethod() {
        console.log('requiredMethod() implemented');
    }
});


const ComplexInterface = GObject.registerClass({
    GTypeName: 'ComplexInterface',
    Requires: [Gio.ListModel, SimpleInterface],
    Properties: {
        'complex-property': GObject.ParamSpec.boolean(
            'complex-property',
            'Complex property',
            'A property that must be implemented',
            GObject.ParamFlags.READABLE,
            true
        ),
    },
}, class ComplexInterface extends GObject.Interface {
    complexMethod() {
        throw new GObject.NotImplementedError();
    }
});


const ComplexImplementation = GObject.registerClass({
    Implements: [Gio.ListModel, SimpleInterface, ComplexInterface],
    Properties: {
        'complex-property': GObject.ParamSpec.override('complex-property',
            ComplexInterface),
        'simple-property': GObject.ParamSpec.override('simple-property',
            SimpleInterface),
    },
}, class ComplexImplementation extends Gio.ListStore {
    get complex_property() {
        return false;
    }

    get simple_property() {
        return true;
    }

    complexMethod() {
        console.log('complexMethod() implemented');
    }

    requiredMethod() {
        console.log('requiredMethod() implemented');
    }
});


const simpleInstance = new SimpleImplementation();

if (simpleInstance instanceof GObject.Object)
    console.log('An instance of a GObject');

if (simpleInstance instanceof SimpleInterface)
    console.log('An instance implementing SimpleInterface');

if (!(simpleInstance instanceof Gio.ListModel))
    console.log('Not an implementation of a list model');


let complexInstance = new ComplexImplementation();

if (complexInstance instanceof GObject.Object &&
    complexInstance instanceof Gio.ListStore)
    console.log('An instance with chained inheritance');

if (complexInstance instanceof Gio.ListModel &&
    complexInstance instanceof SimpleInterface &&
    complexInstance instanceof ComplexInterface)
    console.log('An instance implementing three interfaces');
