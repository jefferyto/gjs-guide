import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';


Gtk.init();

const invisibleLabel = new Gtk.Label({
    visible: false,
});
let visible;

// Three different ways to get or set properties
visible = invisibleLabel.visible;
visible = invisibleLabel['visible'];
visible = invisibleLabel.get_visible();

invisibleLabel.visible = false;
invisibleLabel['visible'] = false;
invisibleLabel.set_visible(false);


const markupLabel = new Gtk.Label({
    label: '<i>Italics</i>',
    use_markup: true,
});
let useMarkup;

// If using native accessors, you can use `underscore_case` or `camelCase`
useMarkup = markupLabel.use_markup;
useMarkup = markupLabel.useMarkup;

// Anywhere the property name is a string, you must use `kebab-case`
markupLabel['use-markup'] = true;
markupLabel.connect('notify::use-markup', () => {});

// Getter and setter functions are always case sensitive
useMarkup = markupLabel.get_use_markup();
markupLabel.set_use_markup(true);


const changingLabel = Gtk.Label.new('Original Label');

const labelId = changingLabel.connect('notify::label', (object, _pspec) => {
    console.log(`New label is "${object.label}"`);
});


const prefsTitle = new Gtk.Label({
    label: 'Preferences',
    css_classes: ['heading'],
});
const prefsBox = new Gtk.Box();

// Bind the visibility of the box and label
prefsTitle.bind_property('visible', prefsBox, 'visible',
    GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL);

// Try to make the properties different
prefsTitle.visible = !prefsBox.visible;

if (prefsTitle.visible === prefsBox.visible)
    console.log('properties are equal!');


const searchEntry = new Gtk.Entry();
const searchButton = new Gtk.Button({
    label: 'Go',
});

searchEntry.bind_property_full('text', searchButton, 'sensitive',
    GObject.BindingFlags.DEFAULT,
    (binding, value) => [true, !!value],
    null);
