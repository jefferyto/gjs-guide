import Gio from 'gi://Gio';


try {
    // Create the process object with `new` and pass the arguments and flags as
    // constructor properties. The process will start when `init()` returns,
    // unless an error is thrown.
    const proc = new Gio.Subprocess({
        argv: ['sleep', '10'],
        flags: Gio.SubprocessFlags.NONE,
    });

    // If the cancellable has already been triggered, the call to `init()` will
    // throw an error and the process will not be started.
    const cancellable = new Gio.Cancellable();

    proc.init(cancellable);

    // Chaining to the cancellable allows you to easily kill the process. You
    // could use the same cancellabe for other related tasks allowing you to
    // cancel them all without tracking them separately.
    //
    // NOTE: this is NOT the standard GObject.connect() function, so you should
    //       consult the documentation if the usage seems odd here.
    let cancelId = 0;

    if (cancellable instanceof Gio.Cancellable)
        cancelId = cancellable.connect(() => proc.force_exit());
} catch (e) {
    logError(e);
}
