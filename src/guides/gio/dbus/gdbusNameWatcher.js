import Gio from 'gi://Gio';


// These two functions are the callbacks for when either the service appears or
// disappears from the bus. At least one of these two functions will be called
// when you first start watching a name.


/**
 * Invoked when the name being watched is known to have to have an owner.
 *
 * This will be called when a process takes ownership of the name, which is to
 * say the service actually became active.
 *
 * @param {Gio.DBusConnection} connection - the connection the name is being
 *     watched on
 * @param {string} name - the name being watched
 * @param {string} nameOwner - the unique name of the owner of the name being
 *     watched
 */
function onNameAppeared(connection, name, nameOwner) {
    console.log(`The well-known name ${name} has been owned by ${nameOwner}`);
}

/**
 * Invoked when the name being watched is known not to have to have an owner.
 *
 * Likewise, this will be invoked when the process that owned the name releases
 * the name.
 *
 * @param {Gio.DBusConnection} connection - ...
 * @param {string} name - ...
 */
function onNameVanished(connection, name) {
    console.log(`The name owner of ${name} has vanished`);
}

// Like signal connections and many other similar APIs, this function returns an
// integer that is later passed to Gio.bus_unwatch_name() to stop watching.
const busWatchId = Gio.bus_watch_name(
    Gio.BusType.SESSION,
    'guide.gjs.Test',
    Gio.BusNameWatcherFlags.NONE,
    onNameAppeared,
    onNameVanished);

Gio.bus_unwatch_name(busWatchId);
