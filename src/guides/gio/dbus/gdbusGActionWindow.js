import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';


// Initialize the GTK environment and prepare an event loop
Gtk.init();
const loop = GLib.MainLoop.new(null, false);


// Create a top-level window
const window = new Gtk.Window({
    title: 'GJS GAction Example',
    default_width: 320,
    default_height: 240,
});
window.connect('close-request', () => loop.quit());

const box = new Gtk.Box({
    orientation: Gtk.Orientation.VERTICAL,
    margin_start: 12,
    margin_end: 12,
    margin_top: 12,
    margin_bottom: 12,
    spacing: 12,
});
window.set_child(box);


// GTK will search upwards in the hierarchy of widgets for the group, so we will
// insert our action group into the top-level window.
const remoteGroup = Gio.DBusActionGroup.get(
    Gio.DBus.session,
    'guide.gjs.Test',
    '/guide/gjs/Test'
);
window.insert_action_group('test', remoteGroup);


// We can now refer to our actions using the group name chosen when inserting
// it and the action name we want to activate
const button = new Gtk.Button({
    label: 'Click Me!',
    action_name: 'test.paramAction',
    action_target: new GLib.Variant('s', 'Button was clicked!'),
});
box.append(button);

const check = new Gtk.CheckButton({
    label: 'Toggle Me!',
    action_name: 'test.stateAction',
});
box.append(check);


// Open up the window
window.present();
await loop.runAsync();
