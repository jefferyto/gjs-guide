import GLib from 'gi://GLib';


const loop = new GLib.MainLoop(null, false);

const idleId = GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
    console.log('This callback will only be invoked once.');

    return GLib.SOURCE_REMOVE;
});

const timeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
    console.log('This callback will be invoked once per second, until removed');

    return GLib.SOURCE_CONTINUE;
});

const removeId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 5, () => {
    console.log('This callback will be invoked once after 5 seconds');

    GLib.Source.remove(timeoutId);

    return GLib.SOURCE_REMOVE;
});

await loop.runAsync();
