const Lang = imports.lang;

const GObject = imports.gi.GObject;


var ExampleSubclass = new Lang.Class({
    Name: 'ExampleSubclass',
    GTypeName: 'ExampleSubclass',
    Signals: {},
    InternalChildren: [],
    Children: [],
    Extends: GObject.Object,
    _init(constructArguments) {
        this.parent(constructArguments);
    },
});
