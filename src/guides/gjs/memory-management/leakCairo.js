import Cairo from 'cairo';
import Gtk from 'gi://Gtk?version=4.0';

Gtk.init();


// Create a drawing area and set a drawing function
const drawingArea = new Gtk.DrawingArea();

drawingArea.set_draw_func((area, cr, _width, _height) => {
    // Perform operations on the surface context

    // Freeing the context before returning from the callback
    cr.$dispose();
});
