import GLib from 'gi://GLib';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';


export default class ExampleExtension extends Extension {
    enable() {
        const indicator = new PanelMenu.Button(0.0, 'MyIndicator', false);
        const randomId = GLib.uuid_string_random();

        Main.panel.addToStatusArea(randomId, indicator);
    }

    disable() {
        // When `enable()` returned, both `indicator` and `randomId` fell out of
        // scope and got collected. However, the panel status area is still
        // holding a reference to the PanelMenu.Button GObject.
        //
        // Each time the extension is disabled/enabled (eg. screen locks/unlocks)
        // a new, unremovable indicator will be added to the panel
    }
}
