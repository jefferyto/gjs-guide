/* GNOME Shell modules are imported with a GResource path */
import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';

/* Your extension's modules are imported with a relative path */
import * as Utils from './utils.js';


/* The default export for `extension.js` and `prefs.js` must be the class */
export default class ExampleExtension extends Extension {
    enable() {
        this._indicator = Utils.createIndicator(this.metadata.name);
        Main.panel.addToStatusArea(this.uuid, this._indicator);
    }

    disable() {
        this._indicator?.destroy();
        this._indicator = null;
    }
}
