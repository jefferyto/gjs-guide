// #region imports
import St from 'gi://St';
// #endregion imports

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

export default class ExampleExtension extends Extension {
    enable() {
        // Create a panel button
        this._indicator = new PanelMenu.Button(0.0, this.metadata.name, false);

        // Add an icon
        const icon = new St.Icon({
            icon_name: 'face-laugh-symbolic',
            style_class: 'system-status-icon',
        });
        this._indicator.add_child(icon);

        const scrollView = new St.ScrollView();

        const section1 = new PopupMenu.PopupMenuSection();

        //  Use add_actor() to add scrollview to  PopupMenuSection
        scrollView.add_actor(section1.actor);

        for (let i = 0; i < 30; i++) {
            const pmItem = new PopupMenu.PopupMenuItem(`This is item ${i}`, {});

            section1.addMenuItem(pmItem);
        }

        this._indicator.menu.box.add_child(scrollView);

        // Add the indicator to the panel
        Main.panel.addToStatusArea(this.uuid, this._indicator);
    }

    disable() {
        this._indicator?.destroy();
        this._indicator = null;
    }
}
